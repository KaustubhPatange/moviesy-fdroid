package com.kpstv.yts.ui.settings

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.core.app.ShareCompat
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.kpstv.yts.AppSettings.PURCHASE_ACCOUNT_ERROR_PREF
import com.kpstv.yts.AppSettings.SHOW_ACCOUNT_ID_PREF
import com.kpstv.yts.R
import com.kpstv.yts.extensions.utils.AppUtils
import com.kpstv.yts.ui.helpers.PremiumHelper
import es.dmoral.toasty.Toasty
import org.json.JSONObject

@Suppress("DEPRECATION")
class AccountSettingFragment : PreferenceFragmentCompat() {

    private lateinit var uid: String

    companion object {
        private const val PERMISSION_CODE = 124
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.account_preference, rootKey)

        findPreference<Preference>(SHOW_ACCOUNT_ID_PREF)?.setOnPreferenceClickListener {
            AppUtils.showThatThisIsADemoApp(requireContext())
            true
        }

        findPreference<Preference>(PURCHASE_ACCOUNT_ERROR_PREF)?.setOnPreferenceClickListener {
            sendErrorEmailPayment()
            true
        }
    }

    private fun sendErrorEmailPayment() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && requireContext()
                .checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), PERMISSION_CODE)
            return
        }

        val puchaseJSON = PremiumHelper.getPurchaseHistoryJSON()
        if (!puchaseJSON.isNullOrEmpty()) {
            val jsonObject = JSONObject(puchaseJSON)

            ShareCompat.IntentBuilder.from(requireActivity())
                .setType("message/rfc822")
                .addEmailTo(getString(R.string.author_mail))
                .setSubject("Moviesy payment error (GPay)")
                .setText("""Purchase premium but not unlocked
                        
Order Id: ${jsonObject.getString("orderId")}
Account Key: ${jsonObject.getString("uid")}
Email: ${jsonObject.getString("email")}""".trimIndent())
                .setChooserTitle("Send")
                .intent.also { startActivity(it) }
        } else Toasty.error(requireContext(), getString(R.string.no_purchase_exist)).show()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_CODE && grantResults.isNotEmpty())
            sendErrorEmailPayment()
    }
}