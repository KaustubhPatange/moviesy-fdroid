package com.kpstv.yts.ui.fragments.sheets

import android.animation.Animator
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import com.kpstv.common_moviesy.extensions.viewBinding
import com.kpstv.yts.R
import com.kpstv.yts.databinding.BottomSheetPurchaseBinding
import com.kpstv.yts.extensions.views.ExtendedBottomSheetDialogFragment
import com.kpstv.common_moviesy.extensions.hide
import com.kpstv.common_moviesy.extensions.show
import com.kpstv.yts.extensions.utils.AppUtils
import com.kpstv.yts.ui.helpers.PremiumHelper
import es.dmoral.toasty.Toasty

class BottomSheetPurchase : ExtendedBottomSheetDialogFragment(R.layout.bottom_sheet_purchase) {

    private val TAG = javaClass.simpleName

    private val binding by viewBinding(BottomSheetPurchaseBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (PremiumHelper.wasPurchased(requireContext())) {
            Toasty.info(requireContext(), getString(R.string.premium_already_unlock)).show()
            dismiss()
        }

        setMultiplePurchaseButtonClicks()

        binding.purchaseButton.setOnClickListener {
            binding.purchaseLayout.hide()
            binding.customMultiplePurchaseLayout.show()
        }
    }

    private fun setMultiplePurchaseButtonClicks() {
        binding.customMultiplePurchase.buttonPaypal.setOnClickListener {
            AppUtils.showThatThisIsADemoApp(requireContext())
        }
        binding.customMultiplePurchase.buttonGpay.setOnClickListener {
            AppUtils.showThatThisIsADemoApp(requireContext())
        }
        binding.customMultiplePurchase.helpButton.setOnClickListener {
            AppUtils.launchUrlIntent(
                requireContext(),
                getString(R.string.payment_help_url)
            )
        }
    }
}