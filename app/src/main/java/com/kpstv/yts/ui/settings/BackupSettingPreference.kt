package com.kpstv.yts.ui.settings

import android.os.Bundle
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.kpstv.yts.R
import com.kpstv.yts.extensions.utils.AppUtils
import com.kpstv.yts.ui.dialogs.ProgressDialog

class BackupSettingPreference : PreferenceFragmentCompat() {

    companion object {
        const val BACKUP_DRIVE_PREF = "backup_drive_pref"
        const val RESTORE_DRIVE_PREF = "restore_drive_pref"
    }

    private var dialog: ProgressDialog? = null

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.backup_preference, rootKey)

        findPreference<Preference>(BACKUP_DRIVE_PREF)?.setOnPreferenceClickListener {
            AppUtils.showThatThisIsADemoApp(requireContext())
            true
        }

        findPreference<Preference>(RESTORE_DRIVE_PREF)?.setOnPreferenceClickListener {
            AppUtils.showThatThisIsADemoApp(requireContext())
            true
        }
    }
}