[![](art/header.png)](https://kaustubhpatange.github.io/Moviesy)

# Moviesy

![build](https://github.com/KaustubhPatange/Moviesy/workflows/build/badge.svg)
![issues](https://img.shields.io/github/issues/KaustubhPatange/Moviesy.svg)

A **_beautiful_** client for [YTS](https://www.google.com/search?q=yts) website which also provides built-in _torrent_, _subtitles_ downloader ❤️

> This is the F-droid version of **Moviesy** which has some limited features (as F-Droid doesn't allow Google related services). But if you want the full version including features like Casting movies to chromecast, etc. be sure to checkout [Github](https://github.com/KaustubhPatange/Moviesy) version.

[![Moviesy App](https://img.shields.io/badge/Download-APK-red.svg?style=for-the-badge&logo=android)](https://github.com/KaustubhPatange/Moviesy/releases/latest)

## Features

- Clean & **beautiful** UI.
- Built-in _movie_ & _subtitle_ downloader
- Supports torrent **streaming**.
- **Watch** movie using in-built player.
- **Cast** to chromecast devices.
- Lot more...

## License

- [The Apache License Version 2.0](https://www.apache.org/licenses/LICENSE-2.0.txt)

```
Copyright 2020 Kaustubh Patange

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
