private object GradlePluginVersion {
    const val KOTLIN = CoreVersion.KOTLIN
    const val ANDROID_GRADLE = "4.1.1"
    const val SAFE_ARGS = CoreVersion.JETPACK_NAVIGATION
    const val HILT = "2.33-beta"
    const val MAVEN_GRADLE = "2.1"
}

object GradlePluginId {
    const val ANDROID_APPLICATION = "com.android.application"
    const val ANDROID_LIBRARY = "com.android.library"
    const val ANDROID_KTX = "android"
    const val ANDROID_EXTENSIONS_KTX = "android.extensions"
    const val KAPT = "kapt"
    const val PARCELIZE = "kotlin-parcelize"
    const val SAFE_ARGS = "androidx.navigation.safeargs.kotlin"
    const val DAGGER_HILT = "dagger.hilt.android.plugin"
    const val APACHE_HTTP = "org.apache.httpcomponents"
    const val MAVEN_PLUGIN = "com.github.dcendents.android-maven"
}

object GradleDependency {
    const val GRADLE_BUILD_TOOLS = "com.android.tools.build:gradle:${GradlePluginVersion.ANDROID_GRADLE}"
    const val KOTLIN_PLUGIN = "org.jetbrains.kotlin:kotlin-gradle-plugin:${GradlePluginVersion.KOTLIN}"
    const val SAFE_ARGS = "androidx.navigation:navigation-safe-args-gradle-plugin:${GradlePluginVersion.SAFE_ARGS}"
    const val DAGGER_HILT = "com.google.dagger:hilt-android-gradle-plugin:${GradlePluginVersion.HILT}"
    const val MAVEN_GRADLE = "com.github.dcendents:android-maven-gradle-plugin:${GradlePluginVersion.MAVEN_GRADLE}"
}