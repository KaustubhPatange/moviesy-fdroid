import kotlin.reflect.full.memberProperties

@Suppress("unused")
object ModuleDependency {
    const val APP = ":app"
    const val PURCHASE = ":purchase"
    const val COMMON = ":common"
    const val AFTER = ":after"
    const val SHEETS = ":sheets"

    fun getAllModules() = ModuleDependency::class.memberProperties
        .filter { it.isConst }
        .map { it.getter.call().toString() }
        .toSet()
}