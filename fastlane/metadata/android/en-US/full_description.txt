The app let's you download and stream HD movies on the go, if this is something you don't approve then you should stop using it. This is indeed a hobby project :)

Features

- Clean & beautiful UI.
- Built-in movie & subtitle downloader
- Supports torrent streaming.
- Watch movie using in-built player.
- Cast to chromecast devices.
- Lot more...